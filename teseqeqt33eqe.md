[
  {
    "PbStructure": {
      "_info_": {
        "var_name": "PbStructure",
        "var_text": "PM3",
        "rights": "22"
      },
      "query_id": {
        "_info_": {
          "rights": "00",
          "var_type": "uint32",
          "rule": "optional",
          "var_name": "query_id",
          "fm_options": {
            "rights": "00"
          },
          "comment": " \r",
          "var_text": "PM3_query_id",
          "stitek": "Dotaz",
          "poznamka": ""
        },
        "query_ida": {
          "_info_": {
            "rights": "00",
            "var_type": "uint32",
            "rule": "optional",
            "var_name": "query_id",
            "fm_options": {
              "rights": "00"
            },
            "comment": " \r",
            "var_text": "PM3_query_id",
            "stitek": "Dotaz",
            "poznamka": ""
          },
          "query_aid": {
            "_info_": {
              "rights": "00",
              "var_type": "uint32",
              "rule": "optional",
              "var_name": "query_id",
              "fm_options": {
                "rights": "00"
              },
              "comment": " \r",
              "var_text": "PM3_query_id",
              "stitek": "Dotaz",
              "poznamka": ""
            },
            "queray_id": {
              "_info_": {
                "rights": "00",
                "var_type": "uint32",
                "rule": "optional",
                "var_name": "query_id",
                "fm_options": {
                  "rights": "00"
                },
                "comment": " \r",
                "var_text": "PM3_query_id",
                "stitek": "Dotaz",
                "poznamka": ""
              },
              "quereqy_id": {
                "_info_": {
                  "rights": "00",
                  "var_type": "uint32",
                  "rule": "optional",
                  "var_name": "query_id",
                  "fm_options": {
                    "rights": "00"
                  },
                  "comment": " \r",
                  "var_text": "PM3_query_id",
                  "stitek": "Dotaz",
                  "poznamka": ""
                }
              }
            }
          }
        }
      }
    }
  }
]